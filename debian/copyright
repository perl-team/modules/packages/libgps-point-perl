Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: GPS-Point
Upstream-Contact: Michael R. Davis
Source: https://metacpan.org/release/GPS-Point

Files: *
Copyright: 2008, Michael R. Davis
License: BSD-3

Files: debian/*
Copyright: 2010, gregor herrmann <gregoa@debian.org>
 2013-2016, Florian Schlichting <fsfs@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: BSD-3
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 Neither the name of the STOP, LLC nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
